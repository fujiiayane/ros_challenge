#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>
#include <iostream>

using namespace std;

///メッセージを受信できたら呼ばれる関数
void msgCallback(const std_msgs::String &chat_msg){
   // 受信したメッセージを表示
    cout << "rcieved: " << chat_msg.data << endl;
}

int main(int argc, char **argv){              // ノードのメイン関数

    ros::init(argc, argv, "chat_listen"); // ノード名の初期化
    ros::NodeHandle nh;                        // ROSシステムとの通信のためのノードのハンドルを宣言
    ros::Subscriber talk_sub = nh.subscribe("chatter", 100, msgCallback);

    ros::spin();

    return 0;
}
