#include "ros/ros.h"
#include "std_msgs/String.h"

#include <sstream>
#include <iostream>

using namespace std;

int main(int argc, char **argv){

    ros::init(argc, argv, "chat_talk");
    ros::NodeHandle node;
    ros::Publisher talk_pub_ = node.advertise<std_msgs::String>("chatter", 1);
    ros::Rate loop_rate(1);
    
    while(ros::ok()){
        std_msgs::String msg;
        msg.data = "hello world";

        cout << "send: " << msg.data << endl;
        talk_pub_.publish(msg);

        ros::spinOnce();
        loop_rate.sleep();
    }

    return 0;
}

